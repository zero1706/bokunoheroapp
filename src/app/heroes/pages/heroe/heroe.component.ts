import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HeroesService } from '../../services/heroes.service';
import { Heroe } from '../../interfaces/heroes.interface';
import{switchMap} from 'rxjs/operators';
@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styles: [
    `
    img{width:100%;
    border-radius:5px;}
    mat-list{

    }
    `
  ]
})
export class HeroeComponent implements OnInit {

  constructor(private heroesServices:HeroesService,private activatedRoute:ActivatedRoute,private router:Router) { }
  heroe!:Heroe;
  ngOnInit(): void {
    // this.activatedRoute.params.subscribe(console.log);
    this.activatedRoute.params.pipe(
      switchMap(({id})=>this.heroesServices.getHeroePorId(id))
    ).subscribe(heroe=>this.heroe=heroe);
  }
  regresar(){
    this.router.navigate(['/heroes/listado']);
  }

}
