import { Component, OnInit } from '@angular/core';
import { HeroesService } from '../../services/heroes.service';
import { Family, Heroe } from '../../interfaces/heroes.interface';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styles: [`
  mat-card{
  margin-top:20px}
  `]
})
export class ListadoComponent implements OnInit {

  constructor(private heroesService:HeroesService) { }
  heroes:Heroe[]=[];
  ngOnInit(): void {
    this.heroesService.getHeroes()
    .subscribe(heroes=>{
      this.heroes=heroes;
    });
  }

}
