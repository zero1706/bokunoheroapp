export interface Heroe {
  id?:          string;
  name:        null | string;
  alias:       null | string;
  affiliation: null | string;
  birthday:    null | string;
  bloodtype:   Bloodtype | null;
  description: null | string;
  fightstyle:  null;
  gender:      Gender | null;
  Eye:         null | string;
  hair:        null | string;
  height:      null | string;
  kanji:       null | string;
  occupation:  null | string;
  quirk:       null | string;
  romaji:      null | string;
  status:      Status | null;
  teams:       null | string;
  images?:      string[];
  epithet:     null | string;
  ages:        Age[] | null;
  family:      Family[] | null;
}

export interface Age {
  age:  string;
  when: string;
}

export enum Bloodtype {
  A = "A",
  Ab = "AB",
  B = "B",
  O = "O",
}

export interface Family {
  id:   string;
  name: string;
}

export enum Gender {
  Female = "Female",
  Male = "Male",
}

export enum Status {
  Active = "Active",
  Alive = "Alive",
  AliveImprisoned = "Alive (Imprisoned)",
  Arrested = "Arrested",
  Deceased = "Deceased",
  DefunctAllMembersImprisoned = "Defunct (all members imprisoned)",
  Disbanded = "Disbanded",
  Imprisoned = "Imprisoned",
  ImprisonedWhiteNomu = "Imprisoned (White Nomu)",
  Inactive = "Inactive",
  StatusUnknown = "Unknown ",
  Unknown = "Unknown",
}
